Source: qt6-svg
Section: libs
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Patrick Franz <deltaone@debian.org>, Lu YaNing <dluyaning@gmail.com>
Build-Depends: cmake,
               debhelper-compat (= 13),
               libgl-dev,
               libvulkan-dev [linux-any],
               libxkbcommon-dev,
               mold,
               ninja-build,
               pkgconf,
               pkg-kde-tools,
               qt6-base-dev (>= 6.8.2~),
               zlib1g-dev
Build-Depends-Indep: qt6-base-dev (>= 6.8) <!nodoc>,
                     qt6-tools-dev (>= 6.8) <!nodoc>
Standards-Version: 4.6.2
Homepage: https://www.qt.io/developers/
Vcs-Browser: https://salsa.debian.org/qt-kde-team/qt6/qt6-svg
Vcs-Git: https://salsa.debian.org/qt-kde-team/qt6/qt6-svg.git
Rules-Requires-Root: no

Package: qt6-svg
Architecture: any
Section: kde
X-Neon-MergedPackage: true
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Replaces: libqt6svg6,
          libqt6svgwidgets6,
Description: Qt 6 SVG library
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 The QtSvg module provides classes for displaying the contents of SVG files.
 .
 Scalable Vector Graphics (SVG) is a language for describing two-dimensional
 graphics and graphical applications in XML.
 .
 This package contains Qt 6 SVG library.

Package: qt6-svg-dev
Architecture: any
Section: kde
X-Neon-MergedPackage: true
Pre-Depends: ${misc:Pre-Depends}
Depends: qt6-base-dev,
         qt6-svg (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Breaks: libqt6svg6-dev (<< 6.4.1~),
Replaces: libqt6svg6-dev,
          qt6-svg-doc-dev,
          qt6-svg-examples,
Description: Qt 6 SVG - development files
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 The QtSvg module provides classes for displaying the contents of SVG files.
 .
 Scalable Vector Graphics (SVG) is a language for describing two-dimensional
 graphics and graphical applications in XML.
 .
 This package contains the header development files used for building Qt 6
 applications using QtSvg library.

Package: qt6-svg-doc
Architecture: all
Section: kde
X-Neon-MergedPackage: true
Pre-Depends: ${misc:Pre-Depends}
Depends: qt6-base-doc,
         qt6-svg-dev,
         ${misc:Depends},
         ${shlibs:Depends}
Replaces: qt6-svg-doc-html,
Description: Qt 6 SVG - development files
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 The QtSvg module provides classes for displaying the contents of SVG files.
 .
 Scalable Vector Graphics (SVG) is a language for describing two-dimensional
 graphics and graphical applications in XML.
 .
 This package contains the documentation files used for to help build Qt 6
 applications using the QtSvg library.

Package: libqt6svg6
Architecture: all
Depends: qt6-svg, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6svg6-dev
Architecture: all
Depends: qt6-svg-dev, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6svgwidgets6
Architecture: all
Depends: qt6-svg, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qt6-svg-doc-dev
Architecture: all
Depends: qt6-svg-dev, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qt6-svg-doc-html
Architecture: all
Depends: qt6-svg-doc, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qt6-svg-examples
Architecture: all
Depends: qt6-svg-dev, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.